Weather CLI App.

Returns temprature, humidity and weather description.

Uses https://openweathermap.org/current for current weather data.
Uses https://ip-api.com/ for geolocation.

Run with npm start or node src/app.

Options:
```
-V, --version              output the version number
-c, --city <value>         City
-z, --zipcode <value>      Zip Code (<zip>,<countryCode>)
-F, --Fahrenheit           Show temprature in F
-C, --Celcius              Show temprature in C (Default)
-i, --import <file path>   Path for the config file
-n, --nogeo                Disable geolocation
-h, --help                 display help for command
```


if no options are given, input prompts will be presented
