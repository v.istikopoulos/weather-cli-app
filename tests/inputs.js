/* eslint-disable no-unused-expressions */
const proxyquire = require('proxyquire');
const chai = require('chai');

const expect = chai.expect;
// const sinon = require('sinon');

chai.use(require('sinon-chai'));

describe('# inputs', () => {
  let module;

  beforeEach(() => {
    module = proxyquire(process.cwd() + '/src/inputs', { './prompts'() {} });
  });

  describe('handleInput', () => {
    it('Should use config file', () => {
        const options = { import: './tests/test.cfg' };
        const myCallback = (path) => {
            expect(path).equals('Valletta,C');
        };
        module(options, myCallback);
    }),
    it('Should use flags', () => {
        const options = { city: 'Some city', zip: 'Some zip' };
        const myCallback = (city) => {
            expect(city).equals('Some city');
        };
        module(options, myCallback);
    });
  });
});
