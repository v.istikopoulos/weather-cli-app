/* eslint-disable no-unused-expressions */
const chai = require('chai');
const sinon = require('sinon');
const { assert } = require('chai');

chai.use(require('sinon-chai'));

describe('# geo', () => {
    let got;
    let myPublicIp;

    beforeEach(() => {
        got = sinon.stub().returns('some city');

        myPublicIp = {
            v4: sinon.stub().returns('some IP')
        };
    });

    describe('geolocation', () => {
        it('Should return a city', async () => {
            const ip = myPublicIp.v4();
            assert.equal(ip, 'some IP');
            const city = got();
            assert.equal(city, 'some city');
        });
    });
});
