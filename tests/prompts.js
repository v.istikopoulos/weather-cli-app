/* eslint-disable no-unused-expressions */
const proxyquire = require('proxyquire');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

chai.use(require('sinon-chai'));

describe('# prompts', () => {
  let module;
  let geo;
  let readline;
  let myReadline;

  beforeEach(() => {
    geo = sinon.stub().resolves('Some city');

    myReadline = {
      on: sinon.stub(),
      question: sinon.stub(),
      write: sinon.stub(),
      close: sinon.stub()
    };

    readline = {
      createInterface: sinon.stub().returns(myReadline)
    };

    module = proxyquire(process.cwd() + '/src/prompts', {
      readline,
      './geo': geo
    });
  });

  describe('questions', () => {
        it('Should ask and get location and units', async () => {
          myReadline.question.callsArgWith(1, 'some value');
          myReadline.on.callsArgWith(1, 'some value');

          await module((location, units) => {
            expect(location).equals('some value');
            expect(units).equals('some value');
          });

          expect(readline.createInterface).calledOnce;
          expect(myReadline.on).calledOnce;
        });
    });
});
