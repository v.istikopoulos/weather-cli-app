const got = require('got');
const fs = require('fs');
const { program } = require('commander');
require('dotenv').config();

const homedir = require('os').homedir();

const varsInput = require('./inputs');

program.version('0.0.1');

program
  .option('-c, --city <value>', 'City')
  .option('-z, --zipcode <value>', 'Zip Code (<zip>,<countryCode>)')
  .option('-F, --Fahrenheit', 'Show temprature in F')
  .option('-C, --Celcius', 'Show temprature in C (Default)')
  .option('-i, --import <file path>', 'Path for the config file')
  .option('-n, --nogeo', 'Disable geolocation');

program.parse(process.argv);
const key = process.env.WEATHER_API_KEY;
const options = program.opts();
const APICall = async (city, units) => {
    fs.writeFile(homedir + '/weather.cfg', city + '|' + units, (err) => {
        if (err) return console.log(err);
    });

    let system = '&units=metric';
    let symbol = '°C';
    if (units === 'F') {
        system = '&units=imperial';
        symbol = '°F';
    }
    let endpoint = `http://api.openweathermap.org/data/2.5/weather?q=${city}${system}&appid=${key}`;

    if (city.match(/^\d/)) {
        endpoint = `http://api.openweathermap.org/data/2.5/weather?zip=${city}${system}&appid=${key}`;
    }

        try {
            const response = await got(endpoint);

            const data = JSON.parse(response.body);
            console.log(data.main.temp + ' ' + symbol + ' - ' + data.main.humidity + '% humidity');
            console.log(data.weather[0].description);
        } catch (error) {
            const message = JSON.parse(error.response.body).message;
            console.log(message);
        }
};

varsInput(options, APICall);
