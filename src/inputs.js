const fs = require('fs');
const prompts = require('./prompts');

module.exports = function handleInput(options, callback) {
    if (options.import) {
        try {
            const data = fs.readFileSync(options.import, 'utf8');
            const fileData = data.split('|');
            callback(fileData[0], fileData[1]);
        } catch (err) {
            console.error(err);
        }
    } else if (options.city || options.zipcode) {
        const units = options.Fahrenheit ? 'F' : 'C';
        callback(options.city || options.zipcode, units);
    } else {
        prompts(callback, options.nogeo);
    }
};
