const got = require('got');
const publicIp = require('public-ip');

module.exports = async function geolocation() {
        try {
            const ip = await publicIp.v4();
            const endpoint = `http://ip-api.com/json/${ip}`;
            const response = await got(endpoint);
            const data = JSON.parse(response.body);
            const city = data.city;
            return city;
        } catch (error) {
            console.error(error);
            process.exit(1);
        }
};
