const readline = require('readline');
const geo = require('./geo');

module.exports = async function questions(callback, nogeo) {
    const ipgeo = nogeo ? '' : await geo();

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    const questionLocation = () => {
        return new Promise((resolve) => {
            rl.question('Where would you like the weather for?\r\nZurich or 8004,ch for example.\r\n> ', resolve);
            rl.write(ipgeo);
        });
    };

    const questionUnits = () => {
        return new Promise((resolve) => {
            rl.question('Celcius or Fahrenheit?\r\nC or F\r\n> ', resolve);
        });
    };

    const location = await questionLocation();
    const units = await questionUnits();

    const closefunc = () => callback(location, units);

    rl.on('close', closefunc);
    rl.close();
};
